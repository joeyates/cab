# cab: compojure-address-book

An example Clojure application designed to give examples of:

* Database connections,
* HTML templating,
* Authentication.

# Development

## Server

```shell
lein run -m cab.core
```

## REPL

```shell
lein repl
```

# Heroku

## Environment Setup

Create a setting so the production app can recognise the environment:

```shell
heroku config:set RING_ENV=production
```

## Deploy

```shell
git push heroku
```

## REPL

```shell
heroku run lein repl
```

# Database

## Migrate

```shell
(use 'cab.models.migrations)
(migrate)
```

## License

Copyright © 2013 Joe Yates

Distributed under the Eclipse Public License, the same as Clojure.


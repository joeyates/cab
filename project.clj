(defproject cab "0.1.0-SNAPSHOT"
  :description "A minimal Compojure Web Application"
  :url "http://mischievous-savannah-4064.herokuapp.com/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [ring/ring-jetty-adapter "1.1.6"]
                 [compojure "1.1.3"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [postgresql "9.1-901.jdbc4"]
                 [org.xerial/sqlite-jdbc "3.7.2"]
                 [hiccup "1.0.2"]]
  :min-lein-version "2.0.0"
  :plugins [[lein-ring "0.8.2"]])


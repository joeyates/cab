(ns cab.config)

(def production?
  (= "production" (System/getenv "RING_ENV")))

(def development?
  (not production?))

(def db-connection-params
  (if production?
    (System/getenv "DATABASE_URL")
    {:classname "org.sqlite.JDBC"
     :subprotocol "sqlite"
     :subname "development.sqlite3"}))


(ns cab.controllers.addresses
  (:use [compojure.core :only (defroutes GET POST)]
        [cab.config :as config]
        [clojure.java.jdbc :as jdbc])
  (:require [cab.models.address :as model]
            [cab.views.addresses :as view]))

(defn index []
  (jdbc/with-connection config/db-connection-params
    (view/index (model/all))))

(defn show [id]
  (jdbc/with-connection config/db-connection-params
    (view/show (model/find (Integer. id)))))

(defn new-address []
  (view/new))

(defn create [params]
  (jdbc/with-connection config/db-connection-params
    (model/create (get params :name) (get params :street))
    (view/index (model/all))))

(defroutes routes
  (GET   "/"                []             (index))
  (GET   "/addresses"       []             (index))
  (GET   "/addresses/:id"   [id]           (show id))
  (GET   "/addresses/new"   []             (new-address))
  (POST  "/addresses"       [& params]     (create params)))


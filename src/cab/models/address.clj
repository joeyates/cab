(ns cab.models.address
  (:require [clojure.java.jdbc :as jdbc]))

(defn all []
  (jdbc/with-query-results results
    ["select * from addresses order by name"]
    (into [] results)))

(defn create [name street]
  (jdbc/insert-values :addresses [:name :street] [name street]))

(defn find [id]
  (jdbc/with-query-results results
    [(format "select * from addresses where id = %d" id)]
    (first results)))


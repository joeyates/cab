(ns cab.models.migrations
  (:use [clojure.java.jdbc :exclude [resultset-seq]]
        [cab.core])
  (:require [cab.config :as config]))

(defn create-addresses-table []
  (with-connection config/db-connection-params
    (create-table :addresses
      [:id :serial "PRIMARY KEY"]
      [:name :varchar "NOT NULL"]
      [:street :varchar "NOT NULL"])))

(defn migrate []
  (print "Creating database structure...") (flush)
  (create-addresses-table)
  (println " done"))


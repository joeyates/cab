(ns cab.core
  (:use [compojure.core :only (defroutes)]
        [ring.adapter.jetty :only [run-jetty]])
  (:require [cab.controllers.addresses :as addresses]
            [cab.config :as config]
            [clojure.java.jdbc :as jdbc]
            [compojure.handler :as handler]
            [compojure.route :as route]))

(defroutes app-routes
  addresses/routes
  (route/not-found "Not Found"))

(def app
  (handler/site app-routes))

(defn -main [& args]
  (let [port (Integer/parseInt (get (System/getenv) "PORT" "8080"))]
    (run-jetty app {:port port})))


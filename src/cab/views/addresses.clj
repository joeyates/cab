(ns cab.views.addresses
  (:use [hiccup.core :only (h)]
        [hiccup.page :only (html5)])
  (:require [cab.views.layout :as layout]))

(defn index [addresses]
  (layout/public "Addresses"
    [:div
      (map
        (fn [address]
          [:p
            (h (:name address))
            (h (:street address))])
        addresses)]
    [:div
      [:a {:href "/addresses/new"} "add"]]))

(defn new []
  (layout/public "Addresses"
    [:form {:method "post" :action "/addresses"}
      [:input {:name "name" :type "text"}]
      [:input {:name "street" :type "text"}]
      [:input {:value "add" :type "submit"}]]))

(defn show [address]
  (layout/public "Addresses"
    [:p
      (h (:name address))
      (h (:street address))]))


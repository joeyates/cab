(ns cab.views.layout
  (:use [hiccup.page :only (html5)]))

(defn public [title & body]
  (html5
    [:head
      [:meta {:charset "utf-8"}]
      [:title title]]
    [:div {:id "content"} body]))

